<?php

namespace WPezSuite\WPezClasses\AdminSearchCustomize;

/**
 * Modify the admin search query
 *
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_where
 */

class ClassAdminSearchCustomize implements InterfaceAdminSearchCustomize {

    public function __construct() {}


    /**
     * use filter: 'posts_join'
     *
     * @param $join
     *
     * @return string
     */
    public function postsJoin( $str_join ) {

        global $wpdb;

        if ( is_search() ) {

            $str_join .= ' LEFT JOIN ' . $wpdb->postmeta . ' ON ' . $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
        }

        return $str_join;
    }


    /**
     * use filter: 'posts_where'
     *
     * @param $str_where
     *
     * @return null|string|string[]
     */
    public function postsWhere( $str_where ) {

        global $pagenow, $wpdb;

        if ( is_search() ) {

            $str_where = preg_replace(
                "/\(\s*" . $wpdb->posts . ".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
                "(" . $wpdb->posts . ".post_title LIKE $1) OR (" . $wpdb->postmeta . ".meta_value LIKE $1)", $str_where );
        }

        return $str_where;
    }

    /**
     * use filter: 'posts_distinct'
     *
     * @param $str_where
     *
     * @return string
     */
    public function postsDistinct( $str_where ) {

        global $wpdb;

        if ( is_search() ) {

            $str_where = "DISTINCT";
        }

        return $str_where;
    }
}