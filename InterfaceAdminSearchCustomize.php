<?php

namespace WPezSuite\WPezClasses\AdminSearchCustomize;

interface InterfaceAdminSearchCustomize {

	public function postsJoin( $str_join );

	public function postsWhere( $str_where );

	public function postsDistinct( $str_where );

}