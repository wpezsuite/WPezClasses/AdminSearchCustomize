## WPezClasses: Admin Search Customize

__Customize the default WordPress admin-side post / CPT search The ezWay.__

The class as configured adds searching against post_meta to the default search.

As inspired by: http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_where

> --
>
> Special thanks to JetBrains (https://www.jetbrains.com/) and PhpStorm (https://www.jetbrains.com/phpstorm/) for their support of OSS and its devotees. 
>
> --

### Simple Example

_Recommended: Use the WPezClasses autoloader (link below)._

      
```     
global $pagenow;

// limit as much as possible when these filters will run
if ( isset( $pagenow ) && $pagenow === 'edit.php' && isset( $_GET['post_type'] ) && $_GET['post_type'] === 'post' ) {

    use WPezSuite\WPezClasses\AdminSearchCustomize\ClassAdminSearchCustomize as ASC;
    $new_asc = new ASC();
 
    add_filter( 'posts_join' , [$new_asc, 'postsJoin']);
    add_filter( 'posts_where' , [$new_asc, 'postsWhere'])
    add_filter( 'posts_distinct' , [$new_asc, 'postsDistinct'])

}
```

Of you can use the included ClassHooks.php as the add_filter()s are all set up and ready to go. 

```     
global $pagenow;

// limit as much as possible when these filters will run
if ( isset( $pagenow ) && $pagenow === 'edit.php' && isset( $_GET['post_type'] ) && $_GET['post_type'] === 'post' ) {

    use WPezSuite\WPezClasses\AdminSearchCustomize\ClassAdminSearchCustomize as ASC;
    $new_asc = new ASC();
    
    use WPezSuite\WPezClasses\AdminSearchCustomize\ClassHooks as Hooks;
    $new_hooks = new Hooks($new_asc);
    $new_hooks->register();

}
```



### FAQ


__1) Why?__

It came up during a project / assignment. Given its usefulness, it seemed like something that should be in the WPezClasses toolbox. 


__2) Can I use this in my plugin or theme?__

Yes, but to be safe, please change the namespace. 


 __3) - I'm not a developer, can we hire you?__
 
Yes, that's always a possibility. If I'm available, and there's a good mutual fit. 


### HELPFUL LINKS

- https://gitlab.com/wpezsuite/WPezClasses/WPezAutoload

- http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_where

- https://codex.wordpress.org/Plugin_API/Filter_Reference/posts_join

- https://developer.wordpress.org/reference/hooks/posts_join/

- https://codex.wordpress.org/Plugin_API/Filter_Reference/posts_where

- https://developer.wordpress.org/reference/hooks/posts_where/

- https://codex.wordpress.org/Plugin_API/Filter_Reference/posts_distinct

- https://developer.wordpress.org/reference/hooks/posts_distinct/

### CHANGE LOG

- v0.0.4 - 22 April 2019
    - Updated: Interface file / name.

- v0.0.3 - 15 April 2019
  - Updated: namespace
  - Added: ClassHooks and its interface
  - Updated: README


- v0.0.2 - 13 Feb 2019
  - Renamed the methods


- v0.0.1 - 13 Feb 2019
  - Let the games begin